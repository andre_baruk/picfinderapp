//
//  DragDropTextView.swift
//  PicFinderApp
//
//  Created by Jay on 9/22/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class DragDropTextView: NSTextField {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
        isHighlighted = false
    }
    
    override func awakeFromNib() {

        registerForDraggedTypes([NSPasteboard.PasteboardType(kUTTypeItem as String)])
    }
    
    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let pasteboard: NSPasteboard = sender.draggingPasteboard
        let urls = pasteboard.readObjects(forClasses: [NSURL.self], options: nil) as? [URL]
        
        if (urls != nil)
        {
            var path = urls![0].description
            if (path.range(of: "file") != nil)
            {
                path = String(path.dropFirst(7))
            }
            self.stringValue = urls![0].relativePath
        }
        return true
    }
    
    func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
        return true
    }
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        super.draggingEntered(sender)
        return NSDragOperation.copy
    }
    
    override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        return true
    }
    
    
    
}

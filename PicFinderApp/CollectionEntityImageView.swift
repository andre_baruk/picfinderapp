//
//  CollectionEntityImageView.swift
//  PicFinderApp
//
//  Created by Jay on 9/19/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class CollectionEntityImageView: NSImageView {

    var isImageSelected = false
    
    override func draw(_ dirtyRect: NSRect) {
        
        if (isImageSelected)
        {
            super.draw(dirtyRect)
            
            NSColor.selectedControlColor.set()
            
            let path = NSBezierPath(rect:bounds)
            path.lineWidth = 5.0
            path.stroke()
        }
        else
        {
          super.draw(dirtyRect)
        }

    }
    
    override func awakeFromNib() {
    }
    
    func flash()
    {
        NSAnimationContext.runAnimationGroup({ (context) in
            context.duration = 0.05
            self.animator().alphaValue = 0
        }, completionHandler: {
            self.animator().alphaValue = 1
        })
    }
    
    func invertSelectedDeselectedImage() {
        isImageSelected = !isImageSelected
        setNeedsDisplay()
    }
    
    func deselectImage() {
        isImageSelected = false
        setNeedsDisplay()
    }
    
    
    /*
    override func mouseDown(with event: NSEvent) {
        
        // MARK: MAIN ANIMATION - FINISHED
        self.resignFirstResponder()
        self.superview!.becomeFirstResponder()
        if (event.clickCount > 1)
        {
            didDoubleClick = true
            NSAnimationContext.runAnimationGroup({ (context) in
                context.duration = 0.05
                 self.animator().alphaValue = 0
            }, completionHandler: {
                 self.animator().alphaValue = 1
            })
     
        }
    
        // MARK: ALTERNATIVE ANIMATION - NOT FINISHED
        /*
         if (event.clickCount > 1)
         {
         let originalSize = NSSize(width: self.frame.size.width, height: self.frame.size.height)
         let animatedSize = NSSize(width: self.frame.size.width + 50, height: self.frame.size.height + 50)
         print("originalSize: \(originalSize)")
         print("animatedSize: \(animatedSize)")
         NSAnimationContext.runAnimationGroup({ (context) in
         
         context.duration = 1
         // self.animator().alphaValue = 0
         let img2 = NSImageView.init(frame: self.frame)
         
         var imageRect:CGRect = CGRect(x: 0, y: 0, width: self.image!.size.width, height: self.image!.size.height)
         let imageRef = self.image!.cgImage(forProposedRect: &imageRect, context: nil, hints: nil)
         img2.image = NSImage.init(cgImage: imageRef!, size: animatedSize)
         
         self.addSubview(img2)
         
         img2.animator().alphaValue = 0
         // img2.setNeedsDisplay()
         // print(img2.image!.size)
         
         }, completionHandler: {
         self.subviews.removeAll()
         //  self.removesu
         // self.animator().alphaValue = 1
         // self.image!.size = originalSize
         //   self.animator().setBoundsSize(originalSize)
         // self.setNeedsDisplay()
         })
         */
    }*/
}

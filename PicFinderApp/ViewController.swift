//
//  ViewController.swift
//  PicFinderApp
//
//  Created by Jay on 9/14/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSCollectionViewDataSource, NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: CustomCollectionView!
    var sliderView: NSSlider?
    
    var baseMultiplier = 1.0
    
    var sliderValue: Double {
        get {
            return UserDefaults.standard.double(forKey: "sliderValue")
        }
        set (value) {
            UserDefaults.standard.set(value, forKey: "sliderValue")
        }
    }
    
    var entities: [PicPathEntity] = []
    var context: NSManagedObjectContext!
    
    var indiceItensMovidosDrag: Set<IndexPath> = []
    
    // MARK: Standard View Controller Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let delegate = NSApplication.shared.delegate as! AppDelegate
        context = delegate.persistentContainer.viewContext
       // print(NSPersistentContainer.defaultDirectoryURL())
        let fetch = PicPathEntity.entityFetchRequest()
        context.perform {
            self.entities = try! fetch.execute()
            self.validateEntitiesOrder(self.entities)
            self.entities.sort(by: { $0.orderNumber < $1.orderNumber })
            self.collectionView.reloadData()
        }
       
        //scaleDownExistingImages()
        configureCollectionView()
        
        collectionView.registerForDraggedTypes([NSPasteboard.PasteboardType(kUTTypeItem as String)])
        collectionView.setDraggingSourceOperationMask(.move, forLocal: true)
    }
    
    override func viewDidAppear() {

        sliderView = self.view.window!.toolbar!.items[3].view as? NSSlider
        if (sliderView != nil)
        {
            sliderView?.action = #selector(self.sliderMoved(slider:))
            sliderView!.objectValue = sliderValue
            baseMultiplier = sliderValue * 2 / 100
        }
    }
    
    // MARK: CollectionView Delegate & Data Source Functions
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return entities.count
    }
    
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize{
        return NSSize(width: 100 + (100 * baseMultiplier), height: 100 + (100 * baseMultiplier))
    }
    
    /*
    func collectionView(_ collectionView: NSCollectionView, updateDraggingItemsForDrag draggingInfo: NSDraggingInfo) {
        print("drag")
    }
    
    func collectionView(_ collectionView: NSCollectionView, canDragItemsAt indexPaths: Set<IndexPath>, with event: NSEvent) -> Bool {
        print("can drag")
        return true
    }
 */
    
    // MARK: Dragging Delegate Functions
    
    func collectionView(_ collectionView: NSCollectionView, pasteboardWriterForItemAt indexPath: IndexPath) -> NSPasteboardWriting? {
        
        let pasteItem = NSPasteboardItem()
        let data = entities[indexPath.item].picture as Data
        pasteItem.setData(data, forType: NSPasteboard.PasteboardType(kUTTypeItem as String))
        
        return pasteItem
    }
    
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forItemsAt indexPaths: Set<IndexPath>) {
        
        indiceItensMovidosDrag = indexPaths
    }
    
    func collectionView(_ collectionView: NSCollectionView, validateDrop draggingInfo: NSDraggingInfo, proposedIndexPath proposedDropIndexPath: AutoreleasingUnsafeMutablePointer<NSIndexPath>, dropOperation proposedDropOperation: UnsafeMutablePointer<NSCollectionView.DropOperation>) -> NSDragOperation {
        
        let colViewItem: CustomCollectionViewItem? = collectionView.item(at: proposedDropIndexPath.pointee as IndexPath) as? CustomCollectionViewItem
        if (colViewItem != nil)
        {
            colViewItem!.deselectImage()
        }
        
        if (draggingInfo.draggingSource == nil)
        {
            return NSDragOperation.init(rawValue: 0)
        }
        
        
        if proposedDropOperation.pointee == NSCollectionView.DropOperation.on {
            proposedDropOperation.pointee = NSCollectionView.DropOperation.before
        }
        
        return NSDragOperation.move
    }
    
    func collectionView(_ collectionView: NSCollectionView, acceptDrop draggingInfo: NSDraggingInfo, indexPath: IndexPath, dropOperation: NSCollectionView.DropOperation) -> Bool {
        
        var returnValue = true
        if indiceItensMovidosDrag.count == 1
        {
            for indice in indiceItensMovidosDrag {
                let properIndexPath = (indexPath.item <= indice.item) ? indexPath : (IndexPath(item: indexPath.item - 1, section: 0))
                collectionView.animator().moveItem(at: indice, to: properIndexPath)
                
                let draggedEntity = entities[indice.item]
                var rightHandEntity: PicPathEntity? = nil
                
                if (indexPath.item < entities.count)
                {
                    rightHandEntity = entities[indexPath.item]
                }
                
                self.reOrderEntities(dragged: draggedEntity, rightHand: rightHandEntity)
                
            }
        }
        else
        {
            returnValue = false
        }
        collectionView.reloadData()
        
        return returnValue
    }
    
    /*
    func collectionView(_ collectionView: NSCollectionView, draggingSession session: NSDraggingSession, endedAt screenPoint: NSPoint, dragOperation operation: NSDragOperation) {
        
        indiceItensMovidosDrag = []
    }
    */
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {

        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CustomCollectionViewItem"), for: indexPath) as! CustomCollectionViewItem
    
        item.picPathEntity = entities[indexPath.item]
        return item
    }
    
    // MARK: UI Action Functions
    
    @IBAction func openAddEntityView(sender: NSButton) {
        let addEntityView = NSStoryboard(name: "Main", bundle: nil).instantiateController(withIdentifier: "AddEditWindow") as! AddEditEntityViewController
        self.presentAsModalWindow(addEntityView)
    }
    
    @IBAction func removeSelectedEntity(sender: NSButton) {
        let visibleItems = collectionView.visibleItems() as! [CustomCollectionViewItem]
        let entityToRemove = visibleItems.first(where: {
            $0.isSelectedByUser
        })?.picPathEntity
        
        if (entityToRemove != nil)
        {
            let delegate = NSApplication.shared.delegate as! AppDelegate
            context = delegate.persistentContainer.viewContext
            
            context.perform {
                self.context.delete(entityToRemove!)
                PicPathEntity.validateEntitiesOrder(self.entities)
                try! self.context.save()
                self.reloadPicPathEntityData()
            }
        }
    }
    
    @objc func sliderMoved(slider: NSSlider)
    {
        sliderValue = slider.objectValue as! Double
        
        baseMultiplier = sliderValue * 2 / 100
       // print(baseMultiplier)
        collectionView.reloadData()
    }
    
    // MARK: Custom Functions
    
    func validateEntitiesOrder(_ localEntities: [PicPathEntity])
    {
        var isOrderCorrupt = false
        var localEntities = localEntities
        localEntities.sort(by: { $0.orderNumber < $1.orderNumber })
        for entity in localEntities
        {
            if (entity.orderNumber != localEntities.index(of: entity)! + 1)
            {
                isOrderCorrupt = true
                break
            }
        }
        
        if (isOrderCorrupt)
        {
            for entity in localEntities
            {
                entity.orderNumber = UInt32(localEntities.index(of: entity)! + 1)
            }
        }
    }
    
    func reOrderEntities (dragged: PicPathEntity, rightHand: PicPathEntity?) {
        
        var reOrderedEntities: [PicPathEntity] = []
        let rightHandProperValue = rightHand != nil ? Int(rightHand!.orderNumber) : entities.count + 1
        
        // if dragged object was from the left and was the smaller order number
        if (rightHand == nil || dragged.orderNumber < rightHand!.orderNumber)
        {
            // add objects left of dragged object
            reOrderedEntities.append(contentsOf:
                entities.filter({ $0.orderNumber < dragged.orderNumber }))
            
            // add objects between dragged object and destination righthand
            reOrderedEntities.append(contentsOf:
                entities.filter({ $0.orderNumber > dragged.orderNumber && $0.orderNumber < rightHandProperValue }))
            
            // add dragged object
            reOrderedEntities.append(dragged)
            
            // add objects right of destination (first value included)
            reOrderedEntities.append(contentsOf:
                entities.filter({ $0.orderNumber >= rightHandProperValue }))
        }
            // if dragged object was from the right and was the bigger order number
        else if (dragged.orderNumber > rightHand!.orderNumber)
        {
            // add objects left of destination
            reOrderedEntities.append(contentsOf:
                entities.filter({ $0.orderNumber < rightHandProperValue }))
            
            // add dragged object
            reOrderedEntities.append(dragged)
            
            // add objects from the right hand destination object not including the dragged object
            reOrderedEntities.append(contentsOf:
                entities.filter({ $0.orderNumber >= rightHandProperValue && $0 != dragged }))
        }
        
        var index: UInt32 = 1
        for entity in reOrderedEntities
        {
            entity.orderNumber = index
            index = index + 1
        }
        context.perform {
            try! self.context.save()
        }
        
        entities = reOrderedEntities
    }
    
    func reloadPicPathEntityData()
    {
        let delegate = NSApplication.shared.delegate as! AppDelegate
        context = delegate.persistentContainer.viewContext
        let fetch = PicPathEntity.entityFetchRequest()
        context.perform {
            self.entities = try! fetch.execute()
            self.collectionView.deselectAllItems()
            self.collectionView.reloadData()
        }
    }
    
    fileprivate func configureCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        //flowLayout.itemSize = NSSize(width: 300.0, height: 300.0)
        flowLayout.sectionInset = NSEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
        flowLayout.minimumInteritemSpacing = 5.0
        flowLayout.minimumLineSpacing = 5.0
        collectionView.collectionViewLayout = flowLayout
       // view.wantsLayer = true
        collectionView.layer?.backgroundColor = NSColor.black.cgColor
    }
    
    func scaleDownExistingImages() {
        
         let fetch = PicPathEntity.entityFetchRequest()
        
         context.perform {
         self.entities = try! fetch.execute()

         for entity in self.entities
         {
         let img = NSImage.init(data: entity.picture as Data)
         
         entity.picture = img!.resize(divider: 3).compressUnderMegaBytes(megabytes: 1)!
         print("converted size is now \(entity.picture.length/1024)")
         }
         try! self.context.save()
         print("complete")
         
         }
    }
}


//
//  DragDropImageView.swift
//  PicFinderApp
//
//  Created by Jay on 9/15/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class DragDropImageView: NSImageView {
    
    var isDragging = false
    var isImageViewEmpty = true
    var imageIsSelected = false
    let filteringOptions = [NSPasteboard.ReadingOptionKey.urlReadingContentsConformToTypes:NSImage.imageTypes]
    
    override func draw(_ dirtyRect: NSRect) {
        if (isDragging)
        {
            super.draw(dirtyRect)
            NSColor.selectedControlColor.set()
            
            let path = NSBezierPath(rect:bounds)
            path.lineWidth = 5.0
            path.stroke()
        }
        else if (isImageViewEmpty)
        {
            let path = NSBezierPath(rect:bounds)
            path.lineWidth = 1.0
            path.stroke()
        }
        else if (imageIsSelected)
        {
            
            super.draw(dirtyRect)
            NSColor.selectedControlColor.set()
            
            let path = NSBezierPath(rect:bounds)
            path.lineWidth = 5.0
            path.stroke()
            
            
        }
        else
        {
            super.draw(dirtyRect)
        }
    }
    
    override func mouseDown(with event: NSEvent) {
        window!.makeFirstResponder(self)
        super.mouseDown(with: event)
        if (self.image != nil)
        {
            
            imageIsSelected = !imageIsSelected
            setNeedsDisplay()
        }
    }
    
    override func keyUp(with event: NSEvent) {
        print(event.keyCode)
       // print(event.characters)
        if (event.keyCode == 51)
        {
            isImageViewEmpty = true
            image = nil
            setNeedsDisplay()
        }
    }
    
    // Empty override for purpose of removing KeyDown system sound
    override func keyDown(with event: NSEvent) {
    }
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        isDragging = true
        setNeedsDisplay()
        return shouldAllowDrag(sender) ? .copy : NSDragOperation()
    }
    
    override func draggingExited(_ sender: NSDraggingInfo?) {
        isDragging = false
        setNeedsDisplay()
    }
    
    override func draggingEnded(_ sender: NSDraggingInfo) {
        
    }
 
    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let pasteboard: NSPasteboard = sender.draggingPasteboard
        let urls = pasteboard.readObjects(forClasses: [NSURL.self], options: nil) as? [URL]
        
        if (urls != nil)
        {
            self.image = NSImage.init(contentsOf: urls![0])
            self.imageScaling = NSImageScaling.scaleProportionallyUpOrDown
            isDragging = false
            isImageViewEmpty = false
        }
        setNeedsDisplay()
        
        return true
    }
    
    func shouldAllowDrag(_ draggingInfo: NSDraggingInfo) -> Bool {
        let pasteboard = draggingInfo.draggingPasteboard
        if pasteboard.canReadObject(forClasses: [NSURL.self], options: filteringOptions)
        {
            return true
        }
        return false
    }
    
}

extension NSPasteboard.PasteboardType {
    
    static let backwardsCompatibleFileURL: NSPasteboard.PasteboardType = {
        
        if #available(OSX 10.13, *) {
            return NSPasteboard.PasteboardType.fileURL
        } else {
            return NSPasteboard.PasteboardType(kUTTypeFileURL as String)
        }
        
    } ()
    
}

//
//  CustomCollectionViewItem.swift
//  PicFinderApp
//
//  Created by Jay on 9/18/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class CustomCollectionViewItem: NSCollectionViewItem {
    
    var isSelectedByUser = false

    var picPathEntity: PicPathEntity! {
        didSet{
            self.imageView?.image = NSImage.init(data: picPathEntity.picture as Data)
            self.imageView?.imageScaling = NSImageScaling.scaleProportionallyUpOrDown
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resignFirstResponder()
    }
    
    override func mouseDown(with event: NSEvent) {
        super.mouseDown(with: event)
        
        if (event.clickCount > 1)
        {
            (imageView as! CollectionEntityImageView).flash()
            picPathEntity.openFolderInFinder()
        }
        
        if (event.clickCount == 1)
        {
            (imageView as! CollectionEntityImageView).invertSelectedDeselectedImage()
            isSelectedByUser = (imageView as! CollectionEntityImageView).isImageSelected
            
            if (isSelectedByUser)
            {
                (self.collectionView as! CustomCollectionView).deselectItems(apartFrom: self)
            }
        }
    }
 
    func deselectImage() {
        isSelectedByUser = false
        (imageView as! CollectionEntityImageView).deselectImage()
    }
    
}

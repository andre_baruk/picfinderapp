//
//  AddEditEntityViewController.swift
//  PicFinderApp
//
//  Created by Jay on 9/15/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class AddEditEntityViewController: NSViewController, NSTextFieldDelegate {
    
    @IBOutlet weak var pathTextView: DragDropTextView!
    @IBOutlet weak var dropImageView: DragDropImageView!
    @IBOutlet weak var fullPathTextView: NSTextField!
    @IBOutlet weak var dropImageTextView: NSTextField!
    @IBOutlet weak var addButton: NSButton!
    
    var timer: Timer! = nil
    var timerShouldContinue = true
    var counter = 0
    var isProperDirEntered = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullPathTextView.stringValue = ""
        pathTextView.delegate = self
        
        self.addObserver(self, forKeyPath: "dropImageView.image", options: .new, context: nil)
        self.addObserver(self, forKeyPath: "pathTextView.stringValue", options: .new, context: nil)
        addButton.isEnabled = false
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        
        if (keyPath == "dropImageView.image")
        {
            let imageObject = change![NSKeyValueChangeKey.newKey] as? NSImage
            if (imageObject != nil)
            {
                self.dropImageTextView.isHidden = true
                if (isProperDirEntered)
                {
                    addButton.isEnabled = true
                }
            }
            else
            {
                self.dropImageTextView.isHidden = false
            }
        }
        else if (keyPath == "pathTextView.stringValue")
        {
            self.controlTextDidChange(Notification.init(name: Notification.Name(rawValue: "")))
            
        }
    }
    
    override func viewDidAppear() {
        view.window!.styleMask.remove(.resizable)
    }
    
    override func mouseDown(with event: NSEvent) {
        view.window!.makeFirstResponder(self.view)
        
        let locationCord = event.locationInWindow
        let dropImgVarea = view.window!.contentView?.hitTest(locationCord)
        
        let isIn = (dropImgVarea as? NSTextField)?.stringValue == "Drop Image Here"
        
        if (isIn)
        {
            // print(locationCord)
            // print (dropImgVarea)
            print("Is in!")
        }
        else
        {
            // print(locationCord)
            // print (dropImgVarea)
            print("not")
        }
    }
    
    func controlTextDidChange(_ obj: Notification) {
        
        addButton.isEnabled = false
        fullPathTextView.stringValue = "..."
        
        if (timerShouldContinue)
        {
            self.timerShouldContinue = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.updateFullPathInfo()
            }
        }
    }
    
    func updateFullPathInfo()
    {
        
        var doesDirExist = ObjCBool(false)
        FileManager.default.fileExists(atPath: pathTextView.stringValue, isDirectory: &doesDirExist)
        if (pathTextView.stringValue.count == 0)
        {
            fullPathTextView.stringValue = ""
            isProperDirEntered = false
        }
        else if (doesDirExist).boolValue
        {
            fullPathTextView.stringValue = pathTextView.stringValue
            fullPathTextView.textColor = NSColor.black
            isProperDirEntered = true
        }
        else
        {
            fullPathTextView.stringValue = "Invalid Path!"
            fullPathTextView.textColor = NSColor.red
            isProperDirEntered = false
        }
        timerShouldContinue = true
        
        if (dropImageView.image != nil && isProperDirEntered)
        {
            addButton.isEnabled = true
        }
        else
        {
            addButton.isEnabled = false
        }
    }
    
    @IBAction func closeWindow(_ sender: Any) {
        self.dismiss(self)
    }
    
    @IBAction func saveEntityAndCloseWindow(_ sender: Any) {
        
        if (dropImageView.image != nil && isProperDirEntered)
        {
            let delegate = AppDelegate.init()
            let context = delegate.persistentContainer.viewContext
            let newEntity = PicPathEntity.init(context: context)
            
            newEntity.path = pathTextView.stringValue
            //newUser.picture = dropImageView.image!.tiffRepresentation! as NSData
        
            //newUser.picture = dropImageView.image!.compressUnderMegaBytes(megabytes: 1)!
            
            //newUser.picture = resize(image: dropImageView.image!, w: Int((dropImageView.image!.size as CGSize).width)/3, h: Int((dropImageView.image!.size as CGSize).height)/3).compressUnderMegaBytes(megabytes: 1)!
            
            newEntity.picture = dropImageView.image!.resize(divider: 3).compressUnderMegaBytes(megabytes: 1)!
            
            print(newEntity.picture.length/1024)
            
            let nextOrderValue = (presentingViewController as! ViewController).entities.count + 1
            let nextOrderValue32 = UInt32(nextOrderValue)
            newEntity.orderNumber = nextOrderValue32
            print("next order number is: \(nextOrderValue)")
            context.perform {
                try! context.save()
            }
            (self.presentingViewController as! ViewController).reloadPicPathEntityData()
            self.dismiss(self)
            
        }
        
    }
    
    func resize(image: NSImage, w: Int, h: Int) -> NSImage {
        let destSize = NSMakeSize(CGFloat(w), CGFloat(h))
        let newImage = NSImage(size: destSize)
        newImage.lockFocus()
        image.draw(in: NSMakeRect(0, 0, destSize.width, destSize.height), from: NSMakeRect(0, 0, image.size.width, image.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        newImage.unlockFocus()
        newImage.size = destSize
        return newImage
    }
    
}


extension NSImage {
    func compressUnderMegaBytes(megabytes: CGFloat) -> NSData? {
        
        var compressionRatio = 1.0
        guard let tiff = self.tiffRepresentation, let imageRep = NSBitmapImageRep(data: tiff) else { return nil }
        var compressedData = imageRep.representation(using: .jpeg, properties: [.compressionFactor : compressionRatio])!
        while CGFloat(compressedData.count) > megabytes * 1024 * 1024 {
            compressionRatio = compressionRatio * 0.9
            compressedData = imageRep.representation(using: .png, properties:  [.compressionFactor : compressionRatio])!
            if compressionRatio <= 0.4 {
                break
            }
        }
        return compressedData as NSData
    }
    
    func resize(divider: Int = 1) -> NSImage {
        let destSize = NSMakeSize(CGFloat(Int(self.size.width)/divider), CGFloat(Int(self.size.height)/divider))
        let newImage = NSImage(size: destSize)
        newImage.lockFocus()
        self.draw(in: NSMakeRect(0, 0, destSize.width, destSize.height), from: NSMakeRect(0, 0, self.size.width, self.size.height), operation: NSCompositingOperation.sourceOver, fraction: CGFloat(1))
        newImage.unlockFocus()
        newImage.size = destSize
        return newImage
    }
}

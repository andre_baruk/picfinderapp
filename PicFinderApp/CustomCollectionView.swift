//
//  CustomCollectionView.swift
//  PicFinderApp
//
//  Created by Jay on 9/19/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//

import Cocoa

class CustomCollectionView: NSCollectionView {
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        // Drawing code here.
    }
    
    func deselectAllItems() {
        let items = self.visibleItems() as! [CustomCollectionViewItem]
        items.forEach({
            $0.deselectImage()
        })
        
       // print(items.count)
    }
    
    func deselectItems(apartFrom item: CustomCollectionViewItem) {
        let items = self.visibleItems() as! [CustomCollectionViewItem]
        let itemsToDeselect = items.filter({
            return $0 != item && $0.isSelectedByUser == true
        })
        
        itemsToDeselect.forEach({
                $0.deselectImage()
        })
    }
    
}

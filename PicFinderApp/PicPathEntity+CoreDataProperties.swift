//
//  PicPathEntity+CoreDataProperties.swift
//  PicFinderApp
//
//  Created by Jay on 9/14/18.
//  Copyright © 2018 Andrzej Baruk. All rights reserved.
//
//

import Cocoa
import Foundation
import CoreData


extension PicPathEntity {

    @nonobjc public class func entityFetchRequest() -> NSFetchRequest<PicPathEntity> {
        return NSFetchRequest<PicPathEntity>(entityName: "PicPathEntity")
    }
    
    func openFolderInFinder() {
        NSWorkspace.shared.openFile(path)
    }
    
    static func validateEntitiesOrder(_ localEntities: [PicPathEntity])
    {
        var isOrderCorrupt = false
        var localEntities = localEntities
        localEntities.sort(by: { $0.orderNumber < $1.orderNumber })
        for entity in localEntities
        {
            if (entity.orderNumber != localEntities.index(of: entity)! + 1)
            {
                isOrderCorrupt = true
                break
            }
        }
        
        if (isOrderCorrupt)
        {
            for entity in localEntities
            {
                entity.orderNumber = UInt32(localEntities.index(of: entity)! + 1)
            }
        }
    }
    
    @NSManaged public var picture: NSData
    @NSManaged public var path: String
    @NSManaged public var orderNumber: UInt32
}
